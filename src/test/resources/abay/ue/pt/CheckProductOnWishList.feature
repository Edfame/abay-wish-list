Feature: Check if the product is on the wish list
  Checks if the product is on the wish list

  Scenario: Check existing product on existing wish list
    Given the service URL
    And the client with username="0" has a wish list
    And productId=0 is on it
    When the client calls /checkProductOnWishList/"0"/0
    Then the client receives 200 and "Product is on the wish list"

  Scenario: Check non-existing product from existing wish list
    Given the service URL
    And the client with username="0" has a wish list
    And productId=0 is not on it
    When the client calls /checkProductOnWishList/"0"/0
    Then the client receives 404 and "Product is not on the wish list"

  Scenario: Check product from non-existing wish list
    Given the service URL
    And the client with username="0" doesn't have a wish list
    When the client calls /checkProductOnWishList/"0"/0
    Then the client receives 400 and "User doesn't have a wish list"
