Feature: List wish list
  List wish list

  Scenario: List client's wish list
    Given the service URL
    And the client with username="0" has a wish list
    And productId=0 is on it
    When the client calls /listWishList with username="0"
    Then the client receives 200 and "{\"username\": 0,\"productsIds\": [0],\"visible\": false}"

  Scenario: List client's non-existing wish list
    Given the service URL
    And the client with username="0" doesn't have a wish list
    When the client calls /listWishList with username="0"
    Then the client receives 400 and "User doesn't have a wish list"
