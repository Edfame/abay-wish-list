Feature: Define wish list as public or private
  Define wish list visibility

  Scenario Outline: Define an existing wish list as public/private
    Given the service URL
    And the client with username="0" has a wish list
    When the client calls /setWishListVisible with value=<visible>
    Then the client receives 200 and "Wish list is now <answer>"

    Examples:
      | visible | answer  |
      | true    | public  |
      | false   | private |


  Scenario Outline: Define a non-existing wish list as public/private
    Given the service URL
    And the client with username="0" doesn't have a wish list
    When the client calls /setWishListVisible with value=<visible>
    Then the client receives 400 and "User doesn't have a wish list"

    Examples:
      | visible |
      | true    |
      | false   |