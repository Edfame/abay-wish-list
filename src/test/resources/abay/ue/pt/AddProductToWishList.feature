Feature: Add product to wish list
  Adds a product to the wish list

  Scenario: Add non-existent product to existent wish list
    Given the service URL
    And the client with username="0" has a wish list
    And productId=0 is not on it
    When the client calls /addProductToWishList with productId=0
    Then the client receives 200 and "Product added"

  Scenario: Add already existing product to existent wish list
    Given the service URL
    And the client with username="0" has a wish list
    And productId=0 is on it
    When the client calls /addProductToWishList with productId=0
    Then the client receives 406 and "Product already exists"

  Scenario: Add product to non-existing wish list
    Given the service URL
    And the client with username="0" doesn't have a wish list
    When the client calls /addProductToWishList with productId=0
    Then the client receives 200 and "Wish list created and product added"