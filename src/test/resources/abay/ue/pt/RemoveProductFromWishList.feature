Feature: Remove product from the wish list
  Remove product from the wish list

  Scenario: Remove existing product from existing wish list
    Given the service URL
    And the client with username="0" has a wish list
    And productId=0 is on it
    When the client calls /removeProductFromWishList with productId=0
    Then the client receives 200 and "Products removed"

  Scenario: Remove non-existing product from existing wish list
    Given the service URL
    And the client with username="0" has a wish list
    And productId=0 is not on it
    When the client calls /removeProductFromWishList with productId=0
    Then the client receives 406 and "One or more products not on the wish list, nothing removed"

  Scenario: Remove product from non-existing wish list
    Given the service URL
    And the client with username="0" doesn't have a wish list
    When the client calls /removeProductFromWishList with productId=0
    Then the client receives 400 and "User doesn't have a wish list"
