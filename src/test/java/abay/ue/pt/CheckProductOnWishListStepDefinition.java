package abay.ue.pt;

import abay.ue.pt.models.WishList;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class CheckProductOnWishListStepDefinition extends StepDefinition {

    @Autowired
    private MockMvc mvc;

    @When("the client calls \\/checkProductOnWishList\\/{string}\\/{long}")
    public void theClientCallsCheckProductOnWishList(String username, Long productId) throws Exception {

        wishList = new WishList(username, productId);

        action = mvc.perform(get(serviceURL + "/checkProductOnWishList/" + username + "/" + productId)
                .contentType(MediaType.APPLICATION_JSON)
                .content(wishList.toString())
                .characterEncoding("utf-8"));

    }
}
