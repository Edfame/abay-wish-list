package abay.ue.pt;

import abay.ue.pt.models.WishList;
import abay.ue.pt.repositories.WishListRepository;
import io.cucumber.java.en.And;
import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import org.hamcrest.Matchers;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import static org.junit.Assert.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;

public class CommonStepDefinitions extends StepDefinition {

   @Autowired
    private WishListRepository wishListRepository;


    @Transactional
    @Given("the service URL")
    public void theServiceURL() {

        serviceURL = "/api/wishlist";

        wishListRepository.deleteAll();

        assertNotNull(serviceURL);

    }

    @And("the client with username={string} has a wish list")
    public void theClientWithIdHasAWishList(String username) {

        StepDefinition.username = username;

        wishList = new WishList(username);

        wishListRepository.save(wishList);

    }

    @And("the client with username={string} doesn't have a wish list")
    public void theClientWithIdDoesntHaveAWishList(String username) {

        StepDefinition.username = username;

        wishList = new WishList(username);

    }

    @Transactional
    @And("productId={long} is not on it")
    public void productIdIsNotOnIt(long productId) {

        wishListRepository.findByUsername(username).ifPresent(userWishList -> {
            assertFalse(userWishList.getProductsIds().contains(productId));
        });
    }

    @Transactional
    @And("productId={long} is on it")
    public void productIdIsOnIt(long productId) {

        wishList.getProductsIds().add(productId);
        wishListRepository.save(wishList);

        wishListRepository.findByUsername(username).ifPresent(userWishList -> {
            assertTrue(userWishList.getProductsIds().contains(productId));
        });
    }

    @Then("the client receives {int} and {string}")
    public void theClientReceivesInt(int status, String message) throws Exception {


        switch (status) {

            case 200:

                action
                        .andExpect(status().isOk())
                        .andExpect(jsonPath("response", Matchers.is(message)))
                        .andExpect(jsonPath("status", Matchers.is(status)));

                break;

            case 406:

                action
                        .andExpect(status().isNotAcceptable())
                        .andExpect(jsonPath("response", Matchers.is(message)))
                        .andExpect(jsonPath("status", Matchers.is(status)));

                break;

            case 400:

                action
                        .andExpect(status().isBadRequest())
                        .andExpect(jsonPath("response", Matchers.is(message)))
                        .andExpect(jsonPath("status", Matchers.is(status)));

                break;

        }

    }
}
