package abay.ue.pt;

import abay.ue.pt.models.WishList;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

public class ListWishListStepDefinition extends StepDefinition {

    @Autowired
    private MockMvc mvc;

    @When("the client calls \\/listWishList with username={string}")
    public void theClientCallsListWishListWithUserId(String username) throws Exception {

        wishList = new WishList(username);

        action = mvc.perform(get(serviceURL + "/listWishList/" + username)
                .contentType(MediaType.APPLICATION_JSON)
                .content(wishList.toString())
                .characterEncoding("utf-8"));
    }
}
