package abay.ue.pt;

import abay.ue.pt.models.WishList;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class AddProductToWishListStepDefinition extends StepDefinition {

    @Autowired
    private MockMvc mvc;


    @When("the client calls \\/addProductToWishList with productId={long}")
    public void theClientCallsAddProductToWishList(long productId) throws Exception {

        wishList = new WishList(username, productId);

        action = mvc.perform(post(serviceURL + "/addProductToWishList")
                .contentType(MediaType.APPLICATION_JSON)
                .content(wishList.toString())
                .characterEncoding("utf-8"));

    }

}
