package abay.ue.pt;

import abay.ue.pt.models.WishList;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.delete;

public class RemoveProductFromWishListStepDefinition extends StepDefinition{

    @Autowired
    private MockMvc mvc;


    @When("the client calls \\/removeProductFromWishList with productId={long}")
    public void theClientCallsRemoveProductFromWishList(long productId) throws Exception {

        wishList = new WishList(username, productId);

        action = mvc.perform(delete(serviceURL + "/removeProductFromWishList")
                .contentType(MediaType.APPLICATION_JSON)
                .content(wishList.toString())
                .characterEncoding("utf-8"));

    }

}
