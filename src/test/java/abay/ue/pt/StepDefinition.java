package abay.ue.pt;

import abay.ue.pt.models.WishList;
import io.cucumber.spring.CucumberContextConfiguration;
import lombok.Data;
import org.junit.runner.RunWith;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.ResultActions;


@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
@CucumberContextConfiguration
@SpringBootTest(classes = ABayWishListSpringApplication.class)
@Data
public class StepDefinition {

    protected static ResultActions action;
    protected static String serviceURL;
    protected static WishList wishList;
    protected static String username;

}

