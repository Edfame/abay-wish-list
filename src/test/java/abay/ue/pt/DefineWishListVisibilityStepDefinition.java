package abay.ue.pt;

import abay.ue.pt.models.WishList;
import io.cucumber.java.ParameterType;
import io.cucumber.java.en.When;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;

public class DefineWishListVisibilityStepDefinition extends StepDefinition{

    @Autowired
    private MockMvc mvc;

    @ParameterType(value = "true|True|TRUE|false|False|FALSE")
    public Boolean booleanValue(String value) {
        return Boolean.valueOf(value);
    }

    @When("the client calls \\/setWishListVisible with value={booleanValue}")
    public void theClientCallsSetWishListVisibleWithValueVisible(boolean visible) throws Exception {

        wishList = new WishList(username, visible);

        action = mvc.perform(post(serviceURL + "/setWishListVisible")
                .contentType(MediaType.APPLICATION_JSON)
                .content(wishList.toString())
                .characterEncoding("utf-8"));

    }
}
