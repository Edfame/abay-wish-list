package abay.ue.pt.repositories;

import abay.ue.pt.models.WishList;
import org.springframework.data.repository.CrudRepository;

import java.util.Optional;

public interface WishListRepository extends CrudRepository<WishList, Long> {
    Optional<WishList> findByUsername(String username);
}
