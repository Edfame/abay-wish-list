package abay.ue.pt.controllers;

import abay.ue.pt.models.WishList;
import abay.ue.pt.services.WishListService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/api/wishlist")
public class WishListController {

    @Autowired
    private WishListService wishListService;

    @PostMapping("/addProductToWishList")
    @ResponseBody
    public ResponseEntity<?> addProductToWishList(@RequestBody WishList bodyWishList) {

        return wishListService.addProductToWishList(bodyWishList);

    }

    @PostMapping("/setWishListVisible")
    @ResponseBody
    public ResponseEntity<?> setWishListVisible(@RequestBody WishList bodyWishList) {

        return wishListService.setWishListVisible(bodyWishList);

    }

    @DeleteMapping("/removeProductFromWishList")
    @ResponseBody
    public ResponseEntity<?> removeProductFromWishList(@RequestBody WishList bodyWishList) {

        return wishListService.removeProductFromWishList(bodyWishList);

    }

    @GetMapping("/listWishList/{username}")
    @ResponseBody
    public ResponseEntity<?> listWishList(@PathVariable String username) {

        return wishListService.listWishList(username);

    }

    @GetMapping("/checkProductOnWishList/{username}/{productId}")
    @ResponseBody
    public ResponseEntity<?> checkProductOnWishList(@PathVariable String username, @PathVariable Long productId) {

        return wishListService.checkProductOnWishList(username, productId);

    }
}
