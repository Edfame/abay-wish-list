package abay.ue.pt.services;

import abay.ue.pt.models.WishList;
import abay.ue.pt.repositories.WishListRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;
import org.json.JSONObject;

import java.util.Optional;

@Service
public class WishListService {

    @Autowired
    private WishListRepository wishListRepository;

    public ResponseEntity<?> addProductToWishList(WishList bodyWishList) {

        JSONObject body = new JSONObject();

        Optional<WishList> wishList = wishListRepository.findByUsername(bodyWishList.getUsername());

        if (wishList.isPresent()) {

            for (long productId : bodyWishList.getProductsIds()) {

                if (wishList.get().getProductsIds().contains(productId)) {

                    body.put("status", 406);
                    body.put("response", "Product already exists");
                    
                    return new ResponseEntity<>(body.toString(), HttpStatus.NOT_ACCEPTABLE);

                }

            }

            wishList.get().getProductsIds().addAll(bodyWishList.getProductsIds());
            wishListRepository.save(wishList.get());

            body.put("status", 200);
            body.put("response", "Product added");

        } else {

            wishListRepository.save(bodyWishList);

            body.put("status", 200);
            body.put("response", "Wish list created and product added");

        }

        return new ResponseEntity<>(body.toString(), HttpStatus.OK);

    }

    public ResponseEntity<?> setWishListVisible(WishList bodyWishList) {

        JSONObject body = new JSONObject();

        Optional<WishList> wishList = wishListRepository.findByUsername(bodyWishList.getUsername());

        if (wishList.isPresent()) {

            wishList.get().setVisible(bodyWishList.isVisible());
            wishListRepository.save(wishList.get());

            String visibility = wishList.get().isVisible() ? "public" : "private";

            body.put("status", 200);
            body.put("response", "Wish list is now " + visibility);

            return new ResponseEntity<>(body.toString(), HttpStatus.OK);

        } else {

            body.put("status", 400);
            body.put("response", "User doesn't have a wish list");

            return new ResponseEntity<>(body.toString(), HttpStatus.BAD_REQUEST);

        }
    }

    public ResponseEntity<?> checkProductOnWishList(String username, Long productId) {

        JSONObject body = new JSONObject();

        Optional<WishList> wishList = wishListRepository.findByUsername(username);

        if (wishList.isPresent()) {

            if (!(wishList.get().getProductsIds().contains(productId))) {

                body.put("status", 404);
                body.put("response", "Product is not on the wish list");

                return new ResponseEntity<>(body.toString(), HttpStatus.NOT_FOUND);

            } else {

                body.put("status", 200);
                body.put("response", "Product is on the wish list");

                return new ResponseEntity<>(body.toString(), HttpStatus.OK);

            }

        } else {

            body.put("status", 400);
            body.put("response", "User doesn't have a wish list");

            return new ResponseEntity<>(body.toString(), HttpStatus.BAD_REQUEST);

        }

    }

    public ResponseEntity<?> listWishList(String username) {

        JSONObject body = new JSONObject();

        Optional<WishList> wishList = wishListRepository.findByUsername(username);

        if (wishList.isPresent()) {

            body.put("status", 200);
            body.put("response", wishList.get().toString());

            return new ResponseEntity<>(body.toString(), HttpStatus.OK);

        } else {

            body.put("status", 400);
            body.put("response", "User doesn't have a wish list");

            return new ResponseEntity<>(body.toString(), HttpStatus.BAD_REQUEST);

        }
    }

    public ResponseEntity<?> removeProductFromWishList(WishList bodyWishList) {

        JSONObject body = new JSONObject();

        Optional<WishList> wishList = wishListRepository.findByUsername(bodyWishList.getUsername());

        if (wishList.isPresent()) {

            for (long productId : bodyWishList.getProductsIds()) {

                if (!(wishList.get().getProductsIds().contains(productId))) {

                    body.put("status", 406);
                    body.put("response", "One or more products not on the wish list, nothing removed");

                    return new ResponseEntity<>(body.toString(), HttpStatus.NOT_ACCEPTABLE);

                }

            }

            wishList.get().getProductsIds().removeAll(bodyWishList.getProductsIds());
            wishListRepository.save(wishList.get());

            body.put("status", 200);
            body.put("response", "Products removed");

            return new ResponseEntity<>(body.toString(), HttpStatus.OK);

        } else {

            body.put("status", 400);
            body.put("response", "User doesn't have a wish list");

            return new ResponseEntity<>(body.toString(), HttpStatus.BAD_REQUEST);

        }

    }

}

