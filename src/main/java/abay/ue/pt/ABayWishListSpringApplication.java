package abay.ue.pt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ABayWishListSpringApplication {

    public static void main(String[] args) {
        SpringApplication.run(ABayWishListSpringApplication.class, args);
    }

}
