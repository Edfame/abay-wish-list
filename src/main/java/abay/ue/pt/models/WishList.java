package abay.ue.pt.models;

import lombok.Data;
import lombok.NoArgsConstructor;

import javax.persistence.Column;
import javax.persistence.ElementCollection;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.validation.constraints.NotNull;
import java.util.ArrayList;
import java.util.List;

@Entity
@Data
@NoArgsConstructor
public class WishList {

    @Id
    @Column(nullable = false, updatable = false)
    private String username;

    @NotNull
    @ElementCollection
    private List<Long> productsIds;
    private boolean visible;

    public WishList(String username) {

        this.username = username;
        this.productsIds = new ArrayList<>();

    }

    public WishList(String username, boolean visible) {

        this.username = username;
        this.visible = visible;

    }

    public WishList(String username, List<Long> productsIds) {

        this.username = username;
        this.productsIds = productsIds;

    }

    public WishList(String username, long productId) {

        this.username = username;
        this.productsIds = new ArrayList<>();
        this.productsIds.add(productId);

    }

    @Override
    public String toString() {
        return "{" +
                "\"username\": " + username + "," +
                "\"productsIds\": " + productsIds + "," +
                "\"visible\": " + visible +
                "}";
    }
}
