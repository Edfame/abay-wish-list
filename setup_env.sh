#!/bin/sh

echo APP_JAR=$APP_JAR >>.env
echo WORKDIR=$WORKDIR >>.env
echo SERVICE_PORT=$SERVICE_PORT >>.env

echo POSTGRES_URL=$POSTGRES_URL >>.env
echo POSTGRES_USER=$POSTGRES_USER >>.env
echo POSTGRES_PASSWORD=$POSTGRES_PASSWORD >>.env
echo POSTGRES_DB=$POSTGRES_DB >>.env

echo SPRING_DATASOURCE_USERNAME=$SPRING_DATASOURCE_USERNAME >>.env
echo SPRING_DATASOURCE_PASSWORD=$SPRING_DATASOURCE_PASSWORD >>.env