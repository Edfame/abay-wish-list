# Abay Wish List

Wish list service for ABay project - https://gitlab.com/pds8/abay
  
---  
## Table of contents :bookmark:
- [API DOCUMENTATION](#api-documentation-pencil)
- [DOCKER INSTRUCTIONS](#docker-whale)
- [PIPELINE](#pipeline-package)

---  
## API DOCUMENTATION :pencil:

(subject do change)

| Endpoint                                              | Method | Description                                       | Body                                         | Status and message meaning                                                                                                                                                                       |  
|-------------------------------------------------------|--------|---------------------------------------------------|----------------------------------------------|--------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------|  
| /api/wishlist/addProductToWishList                    | POST   | Adds the product to the user's wish list.         | ```{"username": String, "productsIds": long}``` | Status: 200, response: "Product added" <br> Status: 406, response: "Product already exists"                                                                                                      |  
| /api/wishlist/setWishListVisibility                   | POST   | Sets the wish list visibility.                    | ```{"username": String, "visible": boolean}```  | Status: 200, response: "Wish list is now public/private" <br> Status: 400, response: "User doesn't have a wish list"                                                                             |  
| /api/wishlist/checkProductOnWishList/username/productId | GET    | Checks if the product is on the user's wish list. | ---                                          | Status: 200, response: "Product is on the wish list" <br> Status: 404, response: "Product is not on the wish list" <br> Status: 400, response: "User doesn't have a wish list"                   |  
| /api/wishlist/listWishList/username                     | GET    | Gets user's wish list.                            | ---                                          | Status: 200, response: `{\"username\": username,\"productsIds\": [productsIds],\"visible\": false/true}`, <br> Status: 400, response: "User doesn't have a wish list"                                |  
| /api/wishlist/removeProductFromWishList               | DELETE | Removes product from user's wish list.            | ```{"username": String, "productsIds": long}``` | Status: 200, response: "Products removed" <br> Status: 406, response: "One or more products not on the wish list, nothing removed" <br> Status: 400, response: "User doesn't have a wish list"   |  
  
---  
## Docker :whale:

Every step that follows must be executed in the **root directory of the project**.

1. Build the project jar with `mvn clean package`
2. Pull and create the Docker images using `docker-compose up --build -d`

---  
# Pipeline :package:

### Stages
The pipepline was implemented by dividing the work that must be done into tree stages:
1. build
2. package
3. deploy

Respectively, the stages are responsible for:
1. Running the tests and building the project
2. Build the Docker image and publish it to `hub.docker.com`
3. Deploy the application to the remote server and set it to run

### Customisation

#### Docker credentials
To customise the place where to push image (registry), the user and the password, the variables `CI_REGISTRY`, `CI_REGISTRY_USER` and `CI_REGISTRY_PASSWORD` should be changed accordingly.

#### Service
Some variables, such as, `SERVICE_PORT`, `POSTGRES_DB`, `POSTGRES_PASSWORD`, `POSTGRES_URL` and `POSTGRES_USER`, allow the customisation of the service. Although, if the value of the variable `APP_JAR` is changed it will cause an error because it is the one outputed by the `build` stage and is later used in the `package` and `deploy` stages. The same occurs to value of the variable `WORKDIR` because in the files `.gitlab-ci.yml`, `deploy.sh` and `docker-compose.prod.yml` the path is *hard coded* which will make the pipeline inconsistent.

If it is intended to change the deploy server, not only the scripts in the files `.gitlab-ci.yml` and `deploy.sh` need to be changed by hand, to the new user and server url, but also the value of the variable `SSH_PRIVATE_KEY` to an appropriate ssh key.


### Problems occured
During the implementation of the pipeline some problems occured, being example of them: a badly defined maven dependency (lombok), accessing the `.env` file in wrong locations, not using the correct name for the image in the `package` stage and errors came up when using the value of the variable `WORKDIR` on the paths of the files `.gitlab-ci.yml`, `deploy.sh` and `docker-compose.prod.yml`.
