# Fetch openjdk image.
FROM openjdk:8-jdk-alpine

ARG WORKDIR
ARG APP_JAR

# Create workdir for the app.
RUN mkdir -p $WORKDIR/app

# Choose the containers workdir.
WORKDIR $WORKDIR/app

# Copy compiled jar to the container.
COPY target/$APP_JAR .

# Run the application inside the container.
CMD java -jar $APP_JAR

# Expose spring's default port.
EXPOSE 8080